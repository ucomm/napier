![Shabazz Napier](screenshot.png)

# Napier

Napier is a WordPress theme for UConn websites. It is built from [Underscores](https://github.com/automattic/_s). It is fairly old and only used on [one site](http://castingcall.uconn.edu/) currently. Brian Daley made it to resemble a theme on Aurora for sites which couldn't exist on the Aurora servers.

**NB This version of the theme has not been fully tested. Expect that it will break. The older version used the following:**
- Bower (for bootstrap-sass)
- Compass
- Ruby
- Browsersync (which I can never figure out how to use...)
I've updated the dependencies to contain everything within composer and npm.

## Usage
### Getting started

NB - If you anticipate needing to use a plugin which needs access to the `vendor` directory (like Castor), map it as follow. In `docker-compose.yml`, find the `web` image and under `volumes` write `- ./vendor:/var/www/html/content/plugins/{plugin_name}/vendor`. This gives both the current theme and plugin the vendor folder.

### To get a project running.
```bash
$ composer install # first time only
$ docker-compose up
```
### Accessing containers
To access a particular docker container, find the container name and then enter an interactive terminal.
```bash
$ docker ps # to get the container name
$ docker exec -it container_name /bin/bash
```
### Debugging Wordpress
Wordpress debug logs can be found inside the web container at `/etc/httpd/logs/error_log`

## Bitbucket
### Creating releases
Assuming you're using git flow, tag the release with the command `git flow release start {version_number}`. Tags must follow the [semver system](http://semver.org/). Follow these steps to complete a release
```bash
git tag # check the current tags/versions on the project
git flow release start {new_tag}
git flow release finish {new_tag}
# in the first screen add new info or just save the commit message as is
# in the second screen type in the same tag you just used and save.
git push --tags && git push origin master
git checkout develop
``` 
Finally re-run the pipeline build on the [satis repo](https://bitbucket.org/ucomm/composer-repository).
### Pipelines
This repo has a bitbucket pipelines (written by Adam Berkowitz) attached in case you wish to create zip file downloads for tags/branches. **You may exclude files/folders from the zip archive by adding them to composer.json. The syntax is the same as gitignore.** To enable pipelines on bitbucket, simply visit the project repo and enable pipelines at `repo -> settings -> Pipelines/settings`.

## Jenkins
In order to ensure automatic pushes to our development server(s) take the following steps
- Create a new [Jenkins project](http://ci.pr.uconn.edu:8080/) (either from scratch or by copying - see the [Castor project](http://ci.pr.uconn.edu:8080/job/Castor%20-%20Push%20to%20Dev%20(Aurora%20Sandbox)/) for an example)
- In bitbucket settings, go to `Settings -> Workflow -> Webhooks` and add a hook to Jenkins at the url `http://ci.pr.uconn.edu:8080/bitbucket-hook/`.
- On the appropriate server, make a new directory with the same name as the project. Otherwise the deployment may fail.

## Using wp-project-boilerplate for a plugin

It would be a good idea to keep UComm/WordPress dependencies in require-dev, and only keep functional dependencies in "require".  That way, your package will export with only the required files for your plugin/theme to function, and not include a full WP install. See the [Castor plugin](https://bitbucket.org/ucomm/castor/src) for an example that uses both.

## Known Issues
- The Docker images don't currently work under Linux. This is an issue related to ip tables.
- ~~Docker does not work. Need a working image for WordPress.~~
- ~~`www/wp-config.php` needs to be edited accordingly.~~
- After a release/version bump re-run the [Composer Repository Pipelines build](https://bitbucket.org/ucomm/composer-repository).