const gulp = require('gulp');
const plumber = require('gulp-plumber');
const autoprefixer = require('gulp-autoprefixer');
const concat = require('gulp-concat');
const jshint = require('gulp-jshint');
const uglify = require('gulp-uglify');
const imagemin = require('gulp-imagemin');
const cache = require('gulp-cache');
const minifycss = require('gulp-minify-css');
const sass = require('gulp-sass');
const rename = require('gulp-rename');

gulp.task('images', function() {
    gulp.src('src/images/**/*')
        .pipe(cache(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true })))
        .pipe(gulp.dest('./images/'));
});

gulp.task('styles', function() {
    gulp.src(['src/sass/style.scss'])
        .pipe(plumber({
            errorHandler: function(error) {
                console.log(error.message);
                this.emit('end');
            }
        }))
        .pipe(sass())
        .pipe(autoprefixer('last 2 versions'))
        .pipe(gulp.dest('./'))
        .pipe(minifycss())
        .pipe(gulp.dest('./'))
});

gulp.task('scripts', function() {
    return gulp.src('src/js/**/*.js')
        .pipe(plumber({
            errorHandler: function(error) {
                console.log(error.message);
                this.emit('end');
            }
        }))
        .pipe(jshint())
        .pipe(jshint.reporter('default'))
        .pipe(concat('main.js'))
        .pipe(gulp.dest('./js/'))
        .pipe(rename({ suffix: '.min' }))
        .pipe(uglify())
        .pipe(gulp.dest('./js/'))
});

gulp.task('default', ['styles', 'scripts'], function() {
    gulp.watch("src/sass/**/*.scss", ['styles']);
    gulp.watch("src/js/**/*.js", ['scripts']);
});

gulp.task('build', ['images', 'styles', 'scripts']);