<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package napier
 */


$titan = TitanFramework::getInstance( wp_get_theme() );


?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<script src="<?php echo get_template_directory_uri() . '/js/modernizr-custom.js' ?>" type="text/javascript" charset="utf-8" async defer></script>
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<script type='text/javascript' src='https://scripts-universityofconn.netdna-ssl.com/cookie-notification.js'></script>
<noscript><p>Our websites may use cookies to personalize and enhance your experience. By continuing without changing your cookie settings, you agree to this collection. For more information, please see our <a href="https://privacy.uconn.edu/university-website-notice/" target="_blank">University Websites Privacy Notice</a>.</p></noscript>

<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'napier' ); ?></a>

    <div id="uconn-banner">
      <div id="uconn-header-container">
        <div class="container">
          <div class="row-fluid">
            <div id="home-link-container">
              <a id="home-link" href="http://uconn.edu/">
                <span id="wordmark">UCONN</span>
                <span id="university-of-connecticut">UNIVERSITY OF CONNECTICUT</span>
              </a>
            </div>
              <div id="button-container">
                <span>
                  <a class="btn" href="http://uconn.edu/search.php" title="Search"><i class="icon-search"></i>
                  </a>
                </span>
                <span>
                  <a class="btn" href="http://uconn.edu/azindex.php" title="AZ Index"><i class="icon-a-z"></i></a>
                </span>
              </div>
          </div>
        </div>
      </div>

      <div id="header-container">
        <div class="container" id="site-header">
          <div class="row-fluid">
            <div>
              <p id="super-title" class="hidden-xs"><a href="<?php echo $titan->getOption( 'uconn_website' ) ?>"><?php echo $titan->getOption( 'uconn_department' ) ?></a></p>
              <h1 id="site-title"><a href="/"><?php echo $titan->getOption( 'uconn_title' ) ?></a></h1>
            </div>
            <div id="search-container" class="hidden-xs">

            <?php if( $titan->getOption( 'uconn_search' ) ): ?>
                <?php get_search_form( true ); ?>
            <?php endif; ?>


            </div>
          </div>
        </div>
        <nav class="navbar navbar-default container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav navbar-nav', 'container_class' => 'collapse navbar-collapse ', 'container_id' => 'navbar-collapse' ) ); ?>
        </nav>
      </div>


    </div>

	<div id="content" class="site-content">
        <div class="container">
