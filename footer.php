<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package napier
 */

?>
        </div> <!-- .container -->
	</div><!-- #content -->
</div><!-- #page -->
<footer id="colophon" class="site-footer clearfix" role="contentinfo">
	<div class="site-info container">
        <ul id="footer-links" class="clearfix">
            <li>
                &copy; <a href="http://uconn.edu">University of Connecticut</a>
            </li>
            <li>
                <a href="http://uconn.edu/disclaimers-privacy-copyright/">Disclaimers, Privacy &amp; Copyright</a>
            </li>
            <li>
                <a href="https://accessibility.uconn.edu">Accessibility</a>
            </li>
            <?php

            // Only display the footer if a menu of "Footer" exists in the site.
            if (wp_get_nav_menu_object('Footer')){
                $defaults = array(
                    'menu'          => 'Footer',
                    'container'       => false,
                    'items_wrap'      => '%3$s',
                    'depth'         => 1,
                    'fallback_cb'       => false
                );
                wp_nav_menu( $defaults );
            }

            ?>
        </ul>
	</div><!-- .site-info -->
</footer><!-- #colophon -->


<?php wp_footer(); ?>

</body>
</html>
