<?php

require_once( 'titan-framework-checker.php' );


add_action( 'tf_create_options', 'mytheme_create_options' );
function mytheme_create_options() {

    $theme_name = wp_get_theme();
	
    $titan = TitanFramework::getInstance( 'Napier' );

    $section = $titan->createThemeCustomizerSection( array(
        'name' => __( 'Theme Settings', 'Napier' )
    ) );

    $section->createOption( array(

        'name' => 'Site Title',

        'id' => 'uconn_title',

        'type' => 'text',

        'desc' => 'Defaults to Wordpress site title',

        'default' => get_bloginfo('name')

    ) );


    $section->createOption( array(

        'name' => 'Department',

        'id' => 'uconn_department',

        'type' => 'text',

        'desc' => 'Name of department or division',

        'placeholder' => 'University Communications'

    ) );


    $section->createOption( array(

        'name' => 'Department Website',

        'id' => 'uconn_website',

        'type' => 'text',

        'desc' => 'URL for your department website',

        'placeholder' => 'http://'

    ) );


    $section->createOption( array(

        'name' => 'Site Search in Banner:',

        'id' => 'uconn_search',

        'type' => 'enable',

        'default' => true


    ) );


}
